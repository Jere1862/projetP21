// Definition de la classe Score_manager
// Nom : No�mie Landry-Boisvert
// Date : 18 fevrier 2016
#include "stdafx.h"
#include "Score_manager.h"

Score_manager::Score_manager()
{
	score = 0;
	nombre_note = 0;
}

Score_manager::Score_manager(double sc, int nb_note)
{
	score = sc;
	nombre_note = nb_note;
}

Score_manager::~Score_manager()
{
}

void Score_manager::ajouter_score(double timing)
{
	score = score + timing;
}

void Score_manager::ajouter_note()
{
	nombre_note = nombre_note + 1;
}

double Score_manager::get_score()
{
	return score;
}

int Score_manager::get_nombre_note()
{
	return nombre_note;
}

int Score_manager::get_score_pourcentage()
{
	int retour;
	
	if (nombre_note != 0)
		retour = (score / nombre_note) * 100;

	else
		retour = 0;

	return retour;
}

void Score_manager::set_score(double sc)
{
	if (sc > 0)
		score = sc;
}

void Score_manager::set_nombre_note(int nb_note)
{
	if (nb_note > 0)
		nombre_note = nb_note;
}
