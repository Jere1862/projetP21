/*
* Declaration de la classe Timing_manager
* Auteur: Cedric Godin (P-21)
* Date: 24 fevrier 2016
*/

#ifndef Timing_manager_h
#define Timing_manager_h

#include "stdafx.h"
#include "Note.h"
#include "Score_manager.h"

#define NOMBRE_NOTES 3	//Nombre de notes
#define SPEED 500	//Duree d'une note en ms

using namespace std;

/*
Classe Timing_manager. Gere les series de notes defilantes
*/
class Timing_manager
{
private:
	Note* _note[NOMBRE_NOTES];	//Contient les series de notes defilantes
	bool _chanson_chargee;	//Indique si une chanson est chargee
	bool _chanson_terminee;	//Indique si la chanson est terminee
	thread* _th_chanson;	//Thread pour jouer la chanson (avancer les notes)

public:
	Timing_manager();	//Constructeur par defaut de Timing_manager
	virtual ~Timing_manager();	//Destructeur de Timing_manager

	int charger_chanson(string nom_chanson);	//Charge une chanson pour la jouer par la suite
	int demarrer_chanson();	//Demarre la chanson, donc le jeu
	double get_timing(int indice_note);		//Retourne le timing actuel d'une note

	int get_nombre_note();	//Retourne le nombre de notes
	Note* get_note(int indice_note);	//Attribue un pointeur sur une note au pointeur nouvelle note
	Score_manager* get_score_manager();	//Retourne un pointeur sur le score manager

	int set_score_manager(Score_manager* score_manager);	//Modifie le pointeur sur le score manager
	void set_chanson_terminee(bool chanson_terminee);	//Indique que la chanson est terminee

	bool chanson_est_chargee();	//Indique si une chanson est chargee
	bool chanson_est_terminee();	//Indique si la chanson est terminee
};

void jouer_chanson(Timing_manager* timing_manager);	//Fonction pour joueur une chanson dans le thread

#endif