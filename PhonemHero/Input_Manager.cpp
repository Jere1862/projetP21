//
//
//  Classe Input_Manager
// 
//  A une fonction Update qui doit etre called par une classe qui gere la logique du jeu
//		ou dans le main loop
#include "stdafx.h"
#include "Input_Manager.h"

using namespace std;

Input_Manager::Input_Manager()
{
	qstate = false;
	wstate = false;
	estate = false;
	inputScoreManager = 0;
}

Input_Manager::Input_Manager(Score_manager *s_m, Timing_manager *t_m)
{
	qstate = false;
	wstate = false;
	estate = false;
	inputScoreManager = s_m;
	timingManager = t_m;
}

Input_Manager::~Input_Manager()
{

}

void Input_Manager::Update_Key_State()
{
	if (GetAsyncKeyState('Q') != 0){ //GetAsyncKeyState retourne letat de la touche Q
		// letat dans larray est ensuite updated
		if (!qstate) {
			qstate = true;
			//effectuer action
			//get_timing
			inputScoreManager->ajouter_score(timingManager->get_timing(0));
			if(TRACE)
				cout << "Q" << endl; //Pour tests
		}
	}
	else{
		qstate = false;
	}
	if (GetAsyncKeyState('W') != 0){
		if (!wstate) {
			wstate = true;
			//effectuer action
			inputScoreManager->ajouter_score(timingManager->get_timing(1));
			if (TRACE)
				cout << "W" << endl;
		}
	}
	else{
		wstate = false;
	}
	if (GetAsyncKeyState('E') != 0){
		if (!estate) {
			estate = true;
			//effectuer action
			inputScoreManager->ajouter_score(timingManager->get_timing(2));
			if (TRACE)
				cout << "E" << endl;
		}
	}
	else{
		estate = false;
	}
}	


Score_manager *Input_Manager::GetScoreManager()
{
	return inputScoreManager;
}

void Input_Manager::SetScoreManager(Score_manager *s_m)
{
	inputScoreManager = s_m;
}


/*bool Input_Manager::GetKeyState(int position) {
	switch (position) {
	case 0: 
		return qstate;
		break;
	case 1: 
		return wstate;
		break;
	case 2:
		return estate;
		break;
	default:
		return 0;
		break;
	}	
}
*/  // FONCTION OBSELETE?