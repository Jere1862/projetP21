/*
* Implementation de la classe Timing_manager
* Auteur: Cedric Godin (P-21)
* Date: 24 fevrier 2016
*/

#include "stdafx.h"
#include "Timing_manager.h"

Timing_manager::Timing_manager()
{
	_chanson_chargee = false;
	_chanson_terminee = false;

	for (int i = 0; i < NOMBRE_NOTES; i++)
		_note[i] = new Note();
}

Timing_manager::~Timing_manager()
{
	for (int i = 0; i < NOMBRE_NOTES; i++)
		delete _note[i];
}

int Timing_manager::charger_chanson(string nom_chanson)
{
	int resultat;
	string nom_fichier;

	for (int i = 0; i < NOMBRE_NOTES; i++) {
		nom_fichier = nom_chanson + to_string(i+1) + ".txt";
		resultat = _note[i]->set_fichier_source(nom_fichier);
		if (resultat != OK)
			return resultat;
	}

	_chanson_chargee = true;
	return OK;
}

int Timing_manager::demarrer_chanson()
{
	if (!_chanson_chargee)
		return ERREUR;

	_th_chanson = new thread(jouer_chanson,this);

	return OK;
}

void jouer_chanson(Timing_manager* timing_manager)
{
	Note* note = nullptr;
	int resultat;

	while (1) {
		for (int i = 0; i < NOMBRE_NOTES; i++) {
			resultat = timing_manager->get_note(i)->next();
			if (resultat != OK) {
				timing_manager->set_chanson_terminee(true);
				return;
			}
		}

		this_thread::sleep_for(chrono::milliseconds(SPEED));
	}
}

double Timing_manager::get_timing(int indice_note)
{
	if (indice_note >= 0 && indice_note < NOMBRE_NOTES)
		return _note[indice_note]->get_timing();

	else
		return ERREUR;
}

int Timing_manager::get_nombre_note()
{
	return NOMBRE_NOTES;
}

Note* Timing_manager::get_note(int indice_note)
{
	if (indice_note >= 0 && indice_note < NOMBRE_NOTES) {
		return _note[indice_note];
	}

	else
		return nullptr;
}

Score_manager* Timing_manager::get_score_manager()
{
	return _note[0]->get_score_manager();
}

int Timing_manager::set_score_manager(Score_manager* score_manager)
{
	for (int i = 0; i < NOMBRE_NOTES; i++) {
		if (_note[i]->set_score_manager(score_manager) != OK)
			return ERREUR;
	}

	return OK;
}

void Timing_manager::set_chanson_terminee(bool chanson_terminee)
{
	_chanson_terminee = chanson_terminee;
}

bool Timing_manager::chanson_est_terminee()
{
	return _chanson_terminee;
}

bool Timing_manager::chanson_est_chargee()
{
	return _chanson_chargee;
}