// PhonemHero.cpp : Defines the entry point for the console application.

#include "stdafx.h"
#include "Input_Manager.h"
#include "Score_manager.h"
#include "Note.h"
#include "Timing_manager.h"

#ifdef __cplusplus__
#include <cstdlib>
#else
#include <stdlib.h>
#endif

using namespace std;

int main()
{
	Score_manager *score_manager = new Score_manager();
	Timing_manager *timing_manager = new Timing_manager();
	Input_Manager *input_manager = new Input_Manager(score_manager,timing_manager);

	timing_manager->set_score_manager(score_manager);
	timing_manager->charger_chanson("test_note");

	cout << "Phonem Hero" << endl;
	cout << "Appuyez sur une touche pour debuter le programme de test..." << endl;

	_getch();

	timing_manager->demarrer_chanson();

	while (!(timing_manager->chanson_est_terminee())){//main game loop pour tester les key inputs

		if (system("CLS")) system("clear");
		cout << "Votre score: " << score_manager->get_score_pourcentage() << "%" << endl;
		cout << "Points: " << score_manager->get_score() << "\tNotes: " << score_manager->get_nombre_note() << endl;
		cout << "\tQ\tW\tE" << endl;

		for (int i = 0; i < 7; i++)
		{
			for (int j = 0; j < timing_manager->get_nombre_note(); j++) {
				cout << "\t" << timing_manager->get_note(j)->get_historique()[i];
			}
			cout << "\n";
		}

		cout << ">>>";
		for (int j = 0; j < timing_manager->get_nombre_note(); j++) {
			cout << "\t" << timing_manager->get_note(j)->get_historique()[7];
		}

		cout << "<<<\n";

		for (int i = 8; i < 10; i++)
		{
			for (int j = 0; j < timing_manager->get_nombre_note(); j++) {
				cout << "\t" << timing_manager->get_note(j)->get_historique()[i];
			}
			cout << "\n";
		}

		input_manager->Update_Key_State(); // Mettre TRACE a 0 dans Input_manager pour disable les cout

		Sleep(50); // refresh rate
	}

	cout << "Partie terminee" << endl;
	system("pause");
	return 0;
}