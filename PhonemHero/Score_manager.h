// Definition de la classe Score_manager
// Nom : No�mie Landry-Boisvert
// Date : 18 fevrier 2016
#pragma once


class Score_manager
{
public :
	Score_manager();
	Score_manager(double sc, int nb_note);
	~Score_manager();
	void ajouter_score(double timing);
	void ajouter_note();
	double get_score();
	int get_nombre_note();
	int get_score_pourcentage();
	void set_score(double sc);
	void set_nombre_note(int nb_note);
private :
	double score;
	int nombre_note;
};