/*
* Implementation de la classe Note
* Auteur: Cedric Godin (P-21)
* Date: 18 fevrier 2016
*/

#include "stdafx.h"
#include "Note.h"

Note::Note()
{
	_nom_note = "Note";
	_chemin_fichier_source = "";
	_fichier_source = new fstream();
	_score_manager = nullptr;
	_trigger = false;
	_input = false;
	for (int i = 0; i < 10;i++)
		_timing[i] = 0;
}

Note::~Note()
{
	_fichier_source->close();
	delete _fichier_source;
}

int Note::next()
{
	if (_fichier_source->good()) {

		for (int i = 9; i > 0; i--)
			_timing[i] = _timing[i - 1];
		*_fichier_source >> _timing[0];

		if (_timing[7] == 1)
			_trigger = true;

		if (_timing[7] == 0 && _trigger) {
			if (_input == false)
				_score_manager -> ajouter_note();

			_input = false;
			_trigger = false;
		}

		return OK;
	}

	else if (_fichier_source->eof())
		return FIN_CHANSON;

	return ERREUR_FICHIER;
}

string Note::get_nom_note()
{
	return _nom_note;
}

string Note::get_chemin_fichier_source()
{
	return _chemin_fichier_source;
}

Score_manager* Note::get_score_manager()
{
	return _score_manager;
}

double Note::get_timing()
{
	if (_input == false && _timing[7]!=0) {
		_input = true;
		_score_manager->ajouter_note();
		return _timing[7];
	}

	return 0;
}

double* Note::get_historique()
{
	return _timing;
}

int Note::set_nom_note(string nom_note)
{
	if (nom_note.length() != 0) {
		_nom_note = nom_note;
		return OK;
	}

	else
		return ERREUR;
}

int Note::set_fichier_source(string chemin_fichier_source)
{
	_fichier_source->open(chemin_fichier_source);

	if (_fichier_source->good()) {
		_chemin_fichier_source = chemin_fichier_source;

		int resultat;
		for (int i = 0; i < 10; i++) {
			resultat = next();
			if (resultat != OK)
				return resultat;
		}
		return OK;
	}

	else
		return ERREUR_FICHIER;
}

int Note::set_score_manager(Score_manager* score_manager)
{
	if (score_manager != nullptr) {
		_score_manager = score_manager;
		return OK;
	}

	else
		return ERREUR;
}

int test_note(string nom_fichier)
{
	cout << "Test de note...\n";
	
	Note* note = new Note();
	Score_manager* score_manager = new Score_manager();
	if (note->set_nom_note("TEST") != OK) {
		cout << "Erreur dans le nom" << endl;
		return ERREUR;
	}

	if (note->set_score_manager(score_manager) != OK) {
		cout << "Erreur score_manager" << endl;
		return ERREUR;
	}

	if (note->set_fichier_source(nom_fichier) != OK) {
		cout << "Erreur fichier" << endl;
		return ERREUR_FICHIER;
	}

	cout << "Note: " << note->get_nom_note() << " cree a partir du fichier \"" << note->get_chemin_fichier_source() << "\"\n";

	do
	{
		for (int i = 0; i < 10; i++)
			cout << note->get_historique()[i] << "\t";

		cout << "Timing: " << note->get_timing() << endl;

	} while (note->next()!=FIN_CHANSON);

	cout << "Chanson terminee." << endl;
	delete note;
	delete score_manager;

	return OK;
}