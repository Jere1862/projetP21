#pragma once
#include <conio.h>
#include <iostream>
#include <string>
#include <Windows.h>

#include "Score_manager.h"
#include "Timing_manager.h"

#define NO_KEYS 3
#define TRACE 0


class Input_Manager
{
public:
	Input_Manager();
	Input_Manager(Score_manager *s_m,Timing_manager *t_m);
	~Input_Manager();
	void Update_Key_State();
	Score_manager *GetScoreManager();
	void SetScoreManager(Score_manager *s_m);
	//bool GetKeyState(int position); Fonction obselete pour le jeu, � utiliser plus tard pour les menus?
private:
	bool qstate, wstate, estate;
	Score_manager *inputScoreManager;
	Timing_manager *timingManager;
};

