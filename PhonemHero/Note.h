/*
* Declaration de la classe Note
* Auteur: Cedric Godin (P-21)
* Date: 18 fevrier 2016
*/

#ifndef note_h
#define note_h

#include "stdafx.h"
#include "Score_manager.h"

#define ERREUR_FICHIER -3	//Erreur de lecture du fichier source
#define ERREUR -1	//erreur generale
#define FIN_CHANSON -2	//Fin de la chanson atteinte
#define OK 0	//Aucune erreur

using namespace std;

/*
Classe note. Represente une des series de notes defilantes
*/
class Note
{
private:
	string _nom_note;	//Nom de la note (pour affichage)
	string _chemin_fichier_source;	//Chemin vers le fichier source
	fstream* _fichier_source;	//Fichier source (contient les valeurs de timing)
	Score_manager* _score_manager;	//Reference pour augmenter le nombre de notes
	double _timing[10];	//Les valeurs courantes du timing de la note
	bool _trigger;	//Si une valeur de timing 1 est passee
	bool _input;	//Si une entree a ete detectee

public:
	Note();	//Constructeur par defaut de Note
	virtual ~Note();	//Destructeur de Note
	double get_timing();	//Retourne le timing actuel
	double* get_historique();	//Retourne l'historique de timing
	int next();	//Avance le curseur au prochain temps

	string get_nom_note();	//Retourne le nom de la note
	string get_chemin_fichier_source();	//Retourne le chemin du fichier source
	Score_manager* get_score_manager();	//Retourne le ScoreManager

	int set_nom_note(string nom_note);	//Change le nom de la note
	int set_fichier_source(string chemin_fichier_source);	//Ouvre un nouveau fichier source
	int set_score_manager(Score_manager* score_manager);	//Change la reference du ScoreManager
};

/*
Fonction pour tester la classe note
*/
int test_note(string nom_fichier);

#endif